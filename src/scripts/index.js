'use strict';

((g) => {
  const mobileOverlay = g.document.querySelector('.menu-dropdown');
  const toggleAuthButtons = g.document.querySelectorAll('.log-in, .log-out');
  const mobileMenuTrigger = g.document.querySelectorAll('button.menu-trigger, .menu-dropdown .close-btn');


  const toggleMobileOverlay = () => {
    mobileOverlay.classList.toggle('hide');
    mobileOverlay.firstElementChild.classList.toggle('active');
  }
  const toggleAuth = () =>
    toggleAuthButtons.forEach(elem => elem.classList.toggle('remove'));

  mobileMenuTrigger.forEach(elem => elem.addEventListener('click', toggleMobileOverlay));
  toggleAuthButtons.forEach(elem => elem.addEventListener('click', toggleAuth));
})(window);