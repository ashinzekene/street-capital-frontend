'use strict'

const gulp       = require('gulp')
const sass       = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const connect    = require('gulp-connect')
const fs         = require('fs')

// file paths for assets
const paths = {
  index: './index.html',
  sass: 'src/sass/**/*.scss',
  script: 'src/scripts/**/*.js'
}

const allTasks = ['server', 'watch', 'js', 'sass',]

gulp.task('sass', () => {
  return gulp.src(paths.sass)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'})
    .on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'))
})

gulp.task('js', () => {
  return gulp.src(paths.script)
    .pipe(gulp.dest('dist/scripts'))
})

gulp.task('server', () => connect.server({livereload: true}) )
gulp.task('reload', () => gulp.src(paths.index).pipe(connect.reload()) )

// handle changes and reload server afterwards.
gulp.task('watch', () => {
  gulp.watch(paths.script, ['js', 'reload'])
  gulp.watch(paths.sass, ['sass', 'reload'])
  gulp.watch(paths.index, ['reload'])
})

gulp.task('default', allTasks)

gulp.task('build', ['js', 'sass']);